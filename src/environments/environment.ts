// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyD1Ill0HEDvPzMHOWtWXd091zStMXmBHoA",
    authDomain: "tal-angular.firebaseapp.com",
    databaseURL: "https://tal-angular.firebaseio.com",
    projectId: "tal-angular",
    storageBucket: "tal-angular.appspot.com",
    messagingSenderId: "144874628314",
    appId: "1:144874628314:web:997e4cc6298e1c003131e3",
    measurementId: "G-ZVZQ555GEB"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
