import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {


  /*
  books=[{title:'Alice in Wonderland', author:'Lewis Carrol', summary:"Lorem Ipsum is simply dummy text of the printing and typesetting industry."},
        {title:'War and Peace', author:'Leo Tolstoy', summary:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout."},
        {title:'The Magic Mountain', author:'Thomas Mann', summary:"Contrary to popular belief, Lorem Ipsum is not simply random text."}];

  public addBooks(){
    setInterval(()=>this.books.push({title:'A New One',author:'New author',summary:'New summary'}),2000);
  }

  public getBooks(){
    const booksObservable = new Observable(obs => {
      setInterval(()=>obs.next(this.books),500)});
      return booksObservable;
  }


*/

bookCollection:AngularFirestoreCollection;


public getbooks(userId){
  this.bookCollection = this.db.collection(`users/${userId}/books`); 
  return this.bookCollection.snapshotChanges().pipe(map(
    collection => collection.map(
      document => {
        const data = document.payload.doc.data();
        data.id = document.payload.doc.id;
        return data;
      }
    )
  ))
}

deleteBook(userid:string,id:string){
  this.db.doc(`users/${userid}/books/${id}`).delete();
}


  constructor(private db:AngularFirestore) { }
}
